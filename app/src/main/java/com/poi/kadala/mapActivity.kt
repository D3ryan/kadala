package com.poi.kadala

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.item_chat.*

class mapActivity: AppCompatActivity(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private var latitude :Double=0.0
    private  var longitude :Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_map)

        val bundle = intent.extras
         latitude= bundle!!.getString("latitude").toString().toDouble()
         longitude= bundle!!.getString("longitude").toString().toDouble()
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val userLocation = LatLng(latitude, longitude)
        mMap.addMarker(MarkerOptions().position(userLocation).title("Mi ubicacion"))
       // mMap.moveCamera(CameraUpdateFactory.newLatLng(userLocation))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation,18f),
            4000,
            null
        )
    }

}