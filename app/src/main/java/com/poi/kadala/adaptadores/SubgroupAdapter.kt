package com.poi.kadala.adaptadores

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.poi.kadala.R
import com.poi.kadala.models.Subgroup
import kotlinx.android.synthetic.main.item_group.view.*


class SubgroupAdapter(val subgroupClick: (Subgroup) -> Unit) :
    RecyclerView.Adapter<SubgroupAdapter.SubgroupViewHolder>() {
    private var subgroup : List<Subgroup> = emptyList()

    fun setData(subgroupList: List<Subgroup>){
        subgroup = subgroupList
        notifyDataSetChanged()
    }

    class SubgroupViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubgroupViewHolder {
        return SubgroupViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_group,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SubgroupViewHolder, position: Int) {
        holder.itemView.btn_subgroup.text = subgroup[position].id
        holder.itemView.btn_subgroup.setOnClickListener{
            subgroupClick(subgroup[position])
        }
    }

    override fun getItemCount(): Int {
        return subgroup.size
    }
}