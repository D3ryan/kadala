package com.poi.kadala.adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.poi.kadala.R
import com.poi.kadala.models.Message
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_chat.view.*
import kotlinx.android.synthetic.main.item_chat_general.view.myMessageLayoutG
import kotlinx.android.synthetic.main.item_chat_general.view.myMessageTextViewG
import kotlinx.android.synthetic.main.item_chat_general.view.otherMessageLayoutG
import kotlinx.android.synthetic.main.item_chat_general.view.tareamateria
import kotlinx.android.synthetic.main.item_chat_general.view.*

class MessageGroupAdapter(private val context: Context,
                          private val user: String,
                          private val itemClickListener: OnLocationClickListener,
                          private val fileClickListener: OnFileClickListener
): RecyclerView.Adapter<MessageGroupAdapter.MessageViewHolder>() {

    private var messages: List<Message> = emptyList()
    interface OnLocationClickListener{
        fun onButtonClick(toString: String, toString1: String)
    }

    interface OnFileClickListener{
        fun OnFileButtonClick(position: Int, fileString : String)
    }
    fun setData(list: List<Message>){
        messages = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        return MessageViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_chat,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val message = messages[position]

        /*if(user == message.from){
            if(message.location.isEmpty()) {
            holder.itemView.myMessageLayoutG.visibility = View.VISIBLE
            holder.itemView.otherMessageLayoutG.visibility = View.GONE
            holder.itemView.myMapLayoutG.visibility= View.GONE
            holder.itemView.otherMapLayoutG.visibility= View.GONE
            holder.itemView.myMessageTextViewG.text = message.message
                }
            else{
                holder.itemView.myMessageLayoutG.visibility = View.GONE
                holder.itemView.myMapLayoutG.visibility = View.VISIBLE
                holder.itemView.otherMessageLayoutG.visibility = View.GONE
                holder.itemView.otherMapLayoutG.visibility = View.GONE

                holder.itemView.MyText.text=message.from
                holder.itemView.buttonMyMap.setOnClickListener{
                    itemClickListener.onButtonClick(message.location[0].toString(), message.location[1].toString())
                }
            }
        } else {
            if(message.location.isEmpty()) {
            holder.itemView.myMessageLayoutG.visibility = View.GONE
            holder.itemView.myMapLayoutG.visibility = View.GONE
            holder.itemView.otherMessageLayoutG.visibility = View.VISIBLE
            holder.itemView.otherMapLayoutG.visibility = View.GONE

            holder.itemView.tareamateria.text = message.message
                }
            else{
                holder.itemView.myMessageLayoutG.visibility = View.GONE
                holder.itemView.myMapLayoutG.visibility = View.GONE
                holder.itemView.otherMessageLayoutG.visibility = View.GONE
                holder.itemView.otherMapLayoutG.visibility = View.VISIBLE

                holder.itemView.OtherText.text=message.from
                holder.itemView.buttonMap.setOnClickListener{
                    itemClickListener.onButtonClick(message.location[0].toString(), message.location[1].toString())
                }
            }
        }*/

        if(user == message.from){
            if(message.location.isEmpty() && message.image.isEmpty() && message.file.isEmpty()) {
                holder.itemView.myMessageLayoutG.visibility = View.VISIBLE
                holder.itemView.myMapLayoutG.visibility = View.GONE
                holder.itemView.otherMessageLayoutG.visibility = View.GONE
                holder.itemView.otherMapLayoutG.visibility = View.GONE
                holder.itemView.myImage.visibility = View.GONE
                holder.itemView.otherImage.visibility = View.GONE
                holder.itemView.myDocument.visibility = View.GONE
                holder.itemView.otherDocument.visibility = View.GONE

                holder.itemView.MyText.text=message.from
                holder.itemView.myMessageTextViewG.text = message.message
            }
            else if (message.location.isNotEmpty()){
                holder.itemView.myMessageLayoutG.visibility = View.GONE
                holder.itemView.myMapLayoutG.visibility = View.VISIBLE
                holder.itemView.otherMessageLayoutG.visibility = View.GONE
                holder.itemView.otherMapLayoutG.visibility = View.GONE
                holder.itemView.myImage.visibility = View.GONE
                holder.itemView.otherImage.visibility = View.GONE
                holder.itemView.myDocument.visibility = View.GONE
                holder.itemView.otherDocument.visibility = View.GONE

                holder.itemView.MyText.text=message.from
                holder.itemView.buttonMyMap.setOnClickListener{
                    itemClickListener.onButtonClick(message.location[0].toString(), message.location[1].toString())
                }
            }
            else if (message.image.isNotEmpty()){
                holder.itemView.myMessageLayoutG.visibility = View.GONE
                holder.itemView.myMapLayoutG.visibility = View.GONE
                holder.itemView.otherMessageLayoutG.visibility = View.GONE
                holder.itemView.otherMapLayoutG.visibility = View.GONE
                holder.itemView.myDocument.visibility = View.GONE
                holder.itemView.otherDocument.visibility = View.GONE

                Picasso.get().load(message.image).into(holder.itemView.iv_myimage)

                holder.itemView.myImage.visibility = View.VISIBLE
                holder.itemView.otherImage.visibility = View.GONE

                holder.itemView.MyTextImage.text = message.from

            }
            else if (message.file.isNotEmpty()){
                holder.itemView.myMessageLayoutG.visibility = View.GONE
                holder.itemView.myMapLayoutG.visibility = View.GONE
                holder.itemView.otherMessageLayoutG.visibility = View.GONE
                holder.itemView.otherMapLayoutG.visibility = View.GONE
                holder.itemView.myImage.visibility = View.GONE
                holder.itemView.otherImage.visibility = View.GONE
                holder.itemView.myDocument.visibility = View.VISIBLE
                holder.itemView.otherDocument.visibility = View.GONE

                holder.itemView.MyTextDocument.text = message.from
                holder.itemView.buttonMyDocument.setOnClickListener {
                    fileClickListener.OnFileButtonClick(position, message.file)
                }

            }
        }
        else {
            if(message.location.isEmpty() && message.image.isEmpty() && message.file.isEmpty()) {

                holder.itemView.myMessageLayoutG.visibility = View.GONE
                holder.itemView.myMapLayoutG.visibility = View.GONE
                holder.itemView.otherMessageLayoutG.visibility = View.VISIBLE
                holder.itemView.otherMapLayoutG.visibility = View.GONE
                holder.itemView.myImage.visibility = View.GONE
                holder.itemView.otherImage.visibility = View.GONE
                holder.itemView.tareamateria.text = message.message

                holder.itemView.myDocument.visibility = View.GONE
                holder.itemView.otherDocument.visibility = View.GONE


            } else if (message.location.isNotEmpty()){
                holder.itemView.myMessageLayoutG.visibility = View.GONE
                holder.itemView.myMapLayoutG.visibility = View.GONE
                holder.itemView.otherMessageLayoutG.visibility = View.GONE
                holder.itemView.otherMapLayoutG.visibility = View.VISIBLE
                holder.itemView.myImage.visibility = View.GONE
                holder.itemView.otherImage.visibility = View.GONE

                holder.itemView.myDocument.visibility = View.GONE
                holder.itemView.otherDocument.visibility = View.GONE

                holder.itemView.OtherText.text=message.from
                holder.itemView.buttonMap.setOnClickListener{
                    itemClickListener.onButtonClick(message.location[0].toString(), message.location[1].toString())
                }
            } else if (message.image.isNotEmpty()){
                holder.itemView.myMessageLayoutG.visibility = View.GONE
                holder.itemView.myMapLayoutG.visibility = View.GONE
                holder.itemView.otherMessageLayoutG.visibility = View.GONE
                holder.itemView.otherMapLayoutG.visibility = View.GONE

                holder.itemView.myDocument.visibility = View.GONE
                holder.itemView.otherDocument.visibility = View.GONE

                Picasso.get().load(message.image).into(holder.itemView.iv_otherimage)

                holder.itemView.myImage.visibility = View.GONE
                holder.itemView.otherImage.visibility = View.VISIBLE

                holder.itemView.OtherTextImage.text=message.from

            } else if (message.file.isNotEmpty()){

                holder.itemView.myMessageLayoutG.visibility = View.GONE
                holder.itemView.myMapLayoutG.visibility = View.GONE
                holder.itemView.otherMessageLayoutG.visibility = View.GONE
                holder.itemView.otherMapLayoutG.visibility = View.GONE
                holder.itemView.myImage.visibility = View.GONE
                holder.itemView.otherImage.visibility = View.GONE

                holder.itemView.myDocument.visibility = View.GONE
                holder.itemView.otherDocument.visibility = View.VISIBLE

                holder.itemView.OtherTextDocument.text=message.from
                holder.itemView.buttonOtherDocument.setOnClickListener {
                    fileClickListener.OnFileButtonClick(position, message.file)
                }

            }
        }

    }

    override fun getItemCount(): Int {
        return messages.size
    }

    class MessageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}