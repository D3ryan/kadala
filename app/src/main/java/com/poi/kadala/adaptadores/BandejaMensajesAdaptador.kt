package com.poi.kadala.adaptadores

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.poi.kadala.R
import com.poi.kadala.models.Usuarios
import kotlinx.android.synthetic.main.busqueda_usuarios_chat.view.*
import kotlinx.android.synthetic.main.usuarios_chats.view.*
import kotlinx.android.synthetic.main.usuarios_chats.view.tv_mensaje_usuario

class BandejaMensajesAdaptador(val userClick: (Usuarios) -> Unit) : RecyclerView.Adapter<BandejaMensajesAdaptador.ChatViewHolder>() {
    private var users: List<Usuarios> = emptyList()

    fun setData(list: List<Usuarios>){
        users = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        return ChatViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.busqueda_usuarios_chat,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.itemView.tv_mensaje_usuario_b.text = users[position].id.toString()
        holder.itemView.tv_nombre_usuario_b.text = users[position].username.toString()

        holder.itemView.setOnClickListener {
            userClick(users[position])
        }
    }

    override fun getItemCount(): Int {
        return users.size
    }


    class ChatViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}
