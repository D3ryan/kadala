package com.poi.kadala

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.poi.kadala.adaptadores.MessageGroupAdapter
import com.poi.kadala.models.Group
import com.poi.kadala.models.Message
import com.poi.kadala.utils.Status
import kotlinx.android.synthetic.main.activity_chat_general.editTextTextPersonName2
import kotlinx.android.synthetic.main.activity_chat_general.rv_mensajes_muro
import kotlinx.android.synthetic.main.activity_chat_general.sendMessageButton
import kotlinx.android.synthetic.main.activity_chat_general.myLocation
import kotlinx.android.synthetic.main.activity_chats.*
import org.jitsi.meet.sdk.JitsiMeetActivity
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions
import java.net.URL
import java.util.*

class ChatGeneralActivity : AppCompatActivity(), MessageGroupAdapter.OnLocationClickListener, MessageGroupAdapter.OnFileClickListener {

    private var chatId=""
    private var name=""

    private val fs = FirebaseFirestoreSettings.Builder().setPersistenceEnabled(true).build()

    private var db= Firebase.firestore.apply {
        this.firestoreSettings = fs
    }

    private var group : Group = Group()
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private val REQUEST_CODE_IMAGE = 1004
    private val REQUEST_CODE_PDF = 1005


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_chats)
        tv_status.setVisibility(View.GONE);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        intent.getStringExtra("chatId")?.let{chatId=it}
        intent.getStringExtra("name")?.let{name=it}
        intent.getStringExtra("groupId")?.let { group.id = it }

        myLocation.text=name

        Toast.makeText(applicationContext, group.id, Toast.LENGTH_SHORT).show()

        if (chatId.isNotEmpty() && name.isNotEmpty()){
            initViews()
        }


        val VideoButton=findViewById<android.widget.ImageButton>(R.id.imageButton7)

        VideoButton.setOnClickListener {
            val options = JitsiMeetConferenceOptions.Builder()
                .setServerURL(URL("https://meet.jit.si"))
                .setRoom(chatId)
                .setAudioMuted(true)
                .setVideoMuted(true)
                .setAudioOnly(false)
                .setWelcomePageEnabled(false)
                .build()

            JitsiMeetActivity.launch(this, options)
        }

    }

    private fun initViews() {
        var user = FirebaseAuth.getInstance().currentUser?.email.toString();
        rv_mensajes_muro.layoutManager= LinearLayoutManager(this)
        rv_mensajes_muro.adapter= MessageGroupAdapter(this,user, this, this)

        sendMessageButton.setOnClickListener { sendMessage() }
        btn_location.setOnClickListener{ sendLocation()}
        btn_image.setOnClickListener { openGalleryForImage() }
        btn_document.setOnClickListener { openGalleryForPDF() }


        val chatRef= db.collection("groupChat").document(chatId)

//        db.enableNetwork().addOnCompleteListener {

        chatRef.collection("messages").orderBy("dob", Query.Direction.ASCENDING)
            .get()
            .addOnSuccessListener { messages->
                val listMessages= messages.toObjects(Message::class.java)
                (rv_mensajes_muro.adapter as MessageGroupAdapter).setData(listMessages)
            }
        chatRef.collection("messages").orderBy("dob", Query.Direction.ASCENDING)
            .addSnapshotListener{messages, error->
                if (error==null){
                    messages?.let {
                        val listMessages= it.toObjects(Message::class.java)
                        (rv_mensajes_muro.adapter as MessageGroupAdapter).setData(listMessages)
                    }
                }
            }
//        }
    }

    private fun sendLocation() {

        val task = fusedLocationProviderClient.lastLocation

        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED
        ){
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),101)
            return
        }
        task.addOnSuccessListener {
            if (it!=null){
                var user= FirebaseAuth.getInstance().currentUser?.email.toString();
                val location= listOf(it.latitude, it.longitude)

                val message =Message(
                    message = "",
                    from = user,
                    location = location
                )

                db.collection("groupChat").document(chatId).collection("messages").document().set(message)

                editTextTextPersonName2.setText("")

            }
        }
    }

    private fun sendMessage() {
        var user = FirebaseAuth.getInstance().currentUser?.email.toString();
        var messageTxt= editTextTextPersonName2.text.toString()
        var messageEspacio= "\n"
        var messageFinal= messageTxt+messageEspacio+user
        val message =Message(
            message = messageFinal,
            from = user
        )

        db.collection("groupChat").document(chatId).collection("messages").document().set(message)

        editTextTextPersonName2.setText("")
    }

    private fun openGalleryForImage(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_CODE_IMAGE)
    }

    private fun openGalleryForPDF(){
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        startActivityForResult(intent, REQUEST_CODE_PDF)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var user= FirebaseAuth.getInstance().currentUser?.email.toString();

        var fileName : String

        var fileURI : Uri?
        var message = Message(
            from = user
        )

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_IMAGE){

            fileName = UUID.randomUUID().toString()
            fileURI = data?.data


            val storageReference = FirebaseStorage.getInstance().getReference("uploads/images/$fileName")
            if (fileURI != null) {
                storageReference.putFile(fileURI).addOnSuccessListener {
                    storageReference.downloadUrl.addOnSuccessListener {
                        fileName = it.toString()
                        message.image = fileName
                        db.collection("groupChat").document(chatId).collection("messages").document().set(message)
                            .addOnSuccessListener {
                                Toast.makeText(this, "Imagen subida con exito", Toast.LENGTH_SHORT).show()
                            }

                    }.addOnFailureListener {
                        Toast.makeText(this, "Fallo en la subida de la imagen", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_PDF){

            fileName = UUID.randomUUID().toString()
            fileURI = data?.data

            val storageReference = FirebaseStorage.getInstance().getReference("uploads/files/$fileName")
            if (fileURI != null) {
                storageReference.putFile(fileURI).addOnSuccessListener {
                    storageReference.downloadUrl.addOnSuccessListener {
                        fileName = it.toString()
                        message.file = fileName

                        db.collection("groupChat").document(chatId).collection("messages").document().set(message)
                            .addOnSuccessListener {
                                Toast.makeText(this, "Archivo subido con exito", Toast.LENGTH_SHORT).show()
                            }
                    }.addOnFailureListener {
                        Toast.makeText(this, "Fallo en la subida del archivo", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Status.setUserOnline(db,  FirebaseAuth.getInstance().currentUser!!)
    }

    override fun onPause() {
        super.onPause()
        Status.setUserOffline(db,  FirebaseAuth.getInstance().currentUser!!)

    }

    override fun onButtonClick(toString: String, toString1: String) {
        val intent= Intent(this,mapActivity::class.java)
        intent.putExtra("latitude", toString)
        intent.putExtra("longitude", toString1)
        startActivity(intent)
    }

    override fun OnFileButtonClick(position: Int, fileString: String) {
        startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(fileString)
            )
        )
    }
}
