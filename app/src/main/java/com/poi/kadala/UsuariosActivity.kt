package com.poi.kadala

import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.poi.kadala.adaptadores.BandejaMensajesAdaptador
import com.poi.kadala.adaptadores.ChatAdapter
import com.poi.kadala.adaptadores.MessageAdapter
import com.poi.kadala.models.Chat
import com.poi.kadala.models.Message
import com.poi.kadala.models.Usuarios
import com.poi.kadala.utils.Encrypt
import com.poi.kadala.utils.Status
import kotlinx.android.synthetic.main.activity_chats.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_usuarios.*
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.fragment_chat.rv_mensajes_bandeja
import java.util.*

class UsuariosActivity : AppCompatActivity() {

    private var db= Firebase.firestore
    private var next=true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_usuarios)

        if (FirebaseAuth.getInstance().currentUser != null) {
            initViews()
        }

    }

    private fun initViews() {

        rv_busqueda_chats.layoutManager= LinearLayoutManager(this)
        rv_busqueda_chats.adapter=
            BandejaMensajesAdaptador{ user->
                usuarioSelected( user )
            }

        //Me traigo todos los usuarios
        val userRef= db.collection("usersByEmail")
            .get()
            .addOnSuccessListener { users ->
                val listChats = users.toObjects(Usuarios::class.java)

                for(item in listChats){
                    if(item.encriptado){
                        item.id= Encrypt.decrypt(item.id).toString()
                        item.username= Encrypt.decrypt(item.username).toString()
                    }
                }

                (rv_busqueda_chats.adapter as BandejaMensajesAdaptador).setData(listChats)
            }

                    db.collection("usersByEmail")
                        .addSnapshotListener{users, error->
                            if (error==null){
                                users?.let {
                                    val listChats= it.toObjects(Usuarios::class.java)

                                    for(item in listChats){
                                        if(item.encriptado){
                                            item.id= Encrypt.decrypt(item.id).toString()
                                            item.username= Encrypt.decrypt(item.username).toString()
                                        }
                                    }

                                    (rv_busqueda_chats.adapter as BandejaMensajesAdaptador).setData(listChats)
                                }
                            }
                        }
    }

    private fun usuarioSelected(usuarioS: Usuarios) {

        var chats1: List<Chat> = emptyList()
        var user1 = FirebaseAuth.getInstance().currentUser?.email.toString();
        var username1=""
        var nameChat=""

        //Realizo una consulta al usuario logueado y me traigo sus chats
        db.collection("usersByEmail").document(user1).collection("chats")
            .get()
            .addOnSuccessListener { chats->
                val listChats = chats.toObjects(Chat::class.java)
                //Aqui si funciona y si me trae los chats que tiene el usuario logueado
                chats1= listChats
                //Tambien sabe cual es el tamaño de esa lista
                var size= chats1.size

                var i=1;
            //Creo un ciclo para ver todos sus chats
            for((indice, item) in chats1.withIndex()){
                val otherUser= usuarioS.id

                var user = FirebaseAuth.getInstance().currentUser?.email.toString();
                //Realizo un array con las dos posibles combinaciones de users en chat
                val possibleChat1= arrayOf(otherUser, user)
                val possibleChat2= arrayOf(user, otherUser)
                //Verifico si los arrays creados son iguales al array en la BD
                val isEqual1= possibleChat1 contentEquals item.users.toTypedArray()
                val isEqual2= possibleChat2 contentEquals item.users.toTypedArray()
                //Si es igual alguna de las opciones significa que ya hay un chat
                if (isEqual1 || isEqual2){
                    db.collection("usersByEmail").document(user1)
                        .get()
                        .addOnSuccessListener { documento->
                        if (documento.exists()){
                            //Me traigo el username del usuario logueado
                            username1= documento.getString("username").toString()
                            var inicio= "Chat: "
                            var guion="-"
                            //Creo el nombre del chat concatenando los usernames
                            nameChat= inicio+username1+guion+usuarioS.username
                            //Voy a la pantalla del chat
                            val intent = Intent( this, ChatsActivity::class.java)
                            intent.putExtra("chatId",item.id)
                            intent.putExtra("name", nameChat)
                            intent.putExtra("username", usuarioS.username)
                            intent.putExtra("otherUserId", usuarioS.id)
                            startActivity(intent)
                        }
                    }
                    //Creo un false porque no existe posible chat ya existente con anterioridad
                    next=false
                    break
                }
                //Como no hay chat existente con ese usuario, creamos uno nuevo en la BD
                if(indice+1==size && next){
                        db.collection("usersByEmail").document(user1)
                        .get()
                        .addOnSuccessListener { documento->
                            if (documento.exists()){
                                username1= documento.getString("username").toString()
                                username1=username1
                                var inicio= "Chat: "
                                var guion="-"
                                nameChat= inicio+username1+guion+usuarioS.username

                                val chatId= UUID.randomUUID().toString()
                                val otherUser= usuarioS.id
                                var user = FirebaseAuth.getInstance().currentUser?.email.toString();
                                val users= listOf(user, otherUser)

                                val chat2= Chat(
                                    id = chatId,
                                    name= nameChat,
                                    users= users
                                )
                                db.collection("chats").document(chatId).set(chat2)
                                db.collection("usersByEmail").document(user).collection("chats").document(chatId).set(chat2)
                                db.collection("usersByEmail").document(otherUser).collection("chats").document(chatId).set(chat2)


                                val intent = Intent( this, ChatsActivity::class.java)
                                intent.putExtra("chatId",chatId)
                                intent.putExtra("name", nameChat)
                                intent.putExtra("username", usuarioS.username)
                                intent.putExtra("otherUserId", usuarioS.id)
                                startActivity(intent)
                            }
                        }
                }
            }
            }
    }

    override fun onResume() {
        super.onResume()
        Status.setUserOnline(db,  FirebaseAuth.getInstance().currentUser!!)
    }

    override fun onPause() {
        super.onPause()
        Status.setUserOffline(db,  FirebaseAuth.getInstance().currentUser!!)

    }


}

