package com.poi.kadala.ui.home

import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GetTokenResult
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.poi.kadala.R
import com.poi.kadala.VolleyRequest
import com.poi.kadala.databinding.FragmentHomeBinding
import com.poi.kadala.models.Usuarios
import com.poi.kadala.utils.Encrypt
import org.json.JSONObject
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    private var db= Firebase.firestore
    private var user =  FirebaseAuth.getInstance().currentUser

    private var userData : Usuarios? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        if(user != null){ initViews() }

        /*
        val user = Firebase.auth.currentUser
        user?.let {


            val userJsonObject = JSONObject()
            userJsonObject.put("uid", user.uid)

            val url = "http://10.0.2.2/KadalaApp/php/userinfo.php"

            val jsonObjectRequest = JsonObjectRequest(
                Request.Method.POST, url, userJsonObject,
                { response ->
                    _binding!!.tvCorreo.text = response.getString("email")
                    _binding!!.tvNombre.text = response.getString("username")
                    _binding!!.tvCarrera.text = response.getString("carrera")
                    Toast.makeText(requireContext(), "Datos cargados", Toast.LENGTH_SHORT).show()
                },
                { error ->
                    Log.e("ERROR", "There was an error: $error")

                })

           VolleyRequest.getInstance(requireContext()).addToRequestQueue(jsonObjectRequest)
       }


*/

        return root
    }

    private fun initViews() {

        db.collection("usersByEmail").document(user?.email.toString())
            .get()
            .addOnSuccessListener { documentSnapshot ->

                userData = documentSnapshot.toObject<Usuarios>()

                if (userData != null) {

                    if(!userData!!.encriptado){
                        binding.tvCarrera.text = userData!!.carrera
                        binding.tvNombre.text = userData!!.username
                        binding.tvCorreo.text = userData!!.id
                        binding.sEncrypt.isChecked = userData!!.encriptado
                        binding.sOnline.isChecked = userData!!.estado
                    } else{

                        binding.tvCorreo.text = Encrypt.decrypt(userData!!.id).toString()
                        binding.tvNombre.text = Encrypt.decrypt(userData!!.username).toString()
                        binding.tvCarrera.text = Encrypt.decrypt(userData!!.carrera).toString()
                        binding.sEncrypt.isChecked = userData!!.encriptado
                        binding.sOnline.isChecked = userData!!.estado
                    }
                }
            }
            .addOnFailureListener { Toast.makeText(context, "Ha habido un error", Toast.LENGTH_SHORT).show() }
            .addOnCompleteListener { initializeSwitchListeners() }
    }

    private fun initializeSwitchListeners() {

        binding.sOnline.setOnCheckedChangeListener { buttonView, isChecked ->
            db.collection("usersByEmail").document(user?.email.toString()).update("estado", isChecked)
        }

        binding.sEncrypt.setOnCheckedChangeListener { buttonView, isChecked ->

            db.collection("usersByEmail").document(user?.email.toString())
                .get().addOnSuccessListener { documentSnapshot ->
                    userData = documentSnapshot.toObject<Usuarios>()
                }
                .addOnCompleteListener {
                    if (isChecked) {
                        db.collection("usersByEmail").document(user?.email.toString())
                            .update("username", Encrypt.encrypt(userData!!.username).toString(),
                                "id", Encrypt.encrypt(userData!!.id).toString(),
                                                 "carrera",  Encrypt.encrypt(userData!!.carrera).toString(),
                                                 "password",  Encrypt.encrypt(userData!!.password).toString(),
                                                 "encriptado", true)
                    } else {
                        db.collection("usersByEmail").document(user?.email.toString())
                            .update("username", Encrypt.decrypt(userData!!.username).toString(),
                            "id", Encrypt.decrypt(userData!!.id).toString(),
                            "carrera",  Encrypt.decrypt(userData!!.carrera).toString(),
                            "password",  Encrypt.decrypt(userData!!.password).toString(),
                            "encriptado", false)
                    }
                }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}

