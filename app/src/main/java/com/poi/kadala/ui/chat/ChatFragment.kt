package com.poi.kadala.ui.chat

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.poi.kadala.ChatsActivity
import com.poi.kadala.UsuariosActivity
import com.poi.kadala.adaptadores.ChatAdapter
import com.poi.kadala.databinding.FragmentChatBinding
import com.poi.kadala.models.Chat
import kotlinx.android.synthetic.main.fragment_chat.*
import java.util.*
import com.google.firebase.auth.FirebaseAuth;

import android.widget.TextView
import com.poi.kadala.MenuActivity


class ChatFragment : Fragment() {

    private var _binding: FragmentChatBinding?=null
    private val binding get()= _binding!!

    private var user=""
    private var userId=""
    private var db= Firebase.firestore

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        fun newInstance() = ChatFragment()
    }

    private lateinit var viewModel: ChatViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChatBinding.inflate(inflater, container, false)
        val view = binding.root

        return view
    }
        /*inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? */

        //return inflater.inflate(R.layout.fragment_chat, container, false)


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ChatViewModel::class.java)

        val txtTexto: TextView = this.tv_nombrealumno

        if (FirebaseAuth.getInstance().currentUser != null) {
            user = FirebaseAuth.getInstance().currentUser?.email.toString();
            userId = FirebaseAuth.getInstance().currentUser?.uid.toString();

            txtTexto.text = user
            initViews()
        }else{
            txtTexto.text = "error"
        }

        binding.imgMensaje.setOnClickListener{
            val intent = Intent (getActivity(), UsuariosActivity::class.java)
            getActivity()?.startActivity(intent)

        }

        binding.imgPrueba.setOnClickListener{
            val intent = Intent (getActivity(), ChatsActivity::class.java)
            getActivity()?.startActivity(intent)

        }


    }

    private fun initViews() {

        rv_mensajes_bandeja.layoutManager= LinearLayoutManager(this@ChatFragment.context)
        rv_mensajes_bandeja.adapter=
            ChatAdapter{ chat->
                chatSelected( chat )
            }
        //Me traigo el usuario con el que estoy logueado
        user = FirebaseAuth.getInstance().currentUser?.email.toString();
        val userRef= db.collection("usersByEmail").document(user)
        //Traer los chats del usuario logueado
        userRef.collection("chats")
            .get()
            .addOnSuccessListener { chats ->
                val listChats = chats.toObjects(Chat::class.java)

                (rv_mensajes_bandeja.adapter as ChatAdapter).setData(listChats)
            }

        userRef.collection("chats")
            .addSnapshotListener{chats, error->
                if (error==null){
                    chats?.let {
                        val listChats= it.toObjects(Chat::class.java)
                        (rv_mensajes_bandeja.adapter as ChatAdapter).setData(listChats)
                    }
                }
            }
    }

    private fun chatSelected(chat: Chat) {
        var otherUserId : String? = null
        val intent= Intent(activity, ChatsActivity::class.java)
        intent.putExtra("chatId", chat.id)
        intent.putExtra("name", chat.name)

        for (otherUserIndex in chat.users.indices){
            if(chat.users[otherUserIndex] != this.user){
                otherUserId = chat.users[otherUserIndex]
                break
            }
        }

        if(otherUserId.isNullOrEmpty()) otherUserId = this.user

        intent.putExtra("otherUserId", otherUserId)

        startActivity(intent)
    }



}