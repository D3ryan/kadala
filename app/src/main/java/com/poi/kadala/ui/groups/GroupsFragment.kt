package com.poi.kadala.ui.groups

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.poi.kadala.ChatGeneralActivity
import com.poi.kadala.CrearGrupoActivity
import com.poi.kadala.UsuariosActivity
import com.poi.kadala.adaptadores.SubgroupAdapter
import com.poi.kadala.databinding.FragmentGroupsBinding
import com.poi.kadala.models.Subgroup
import com.poi.kadala.models.Usuarios
import com.poi.kadala.models.groupChatData
import com.poi.kadala.utils.Encrypt


class GroupsFragment : Fragment() {

    private lateinit var viewModel: GroupsViewModel
    private var user = FirebaseAuth.getInstance().currentUser
    private var db = Firebase.firestore
    private var userData : Usuarios = Usuarios()

    private var _binding: FragmentGroupsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(GroupsViewModel::class.java)

        _binding = FragmentGroupsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        if (FirebaseAuth.getInstance().currentUser != null) {
            initViews()
        }

        binding.btnAgregarGrupo.setOnClickListener{
            val intent = Intent (getActivity(), CrearGrupoActivity::class.java)
            getActivity()?.startActivity(intent)

        }

        return root
    }

    private fun initViews() {
        binding.rvSubgroups.layoutManager = LinearLayoutManager(context)
        binding.rvSubgroups.adapter = SubgroupAdapter{ subgroup -> subgroupSelected( subgroup ) }

        db.collection("usersByEmail").document(user!!.email.toString()).get()
            .addOnSuccessListener { userDocument ->
                val userRawData = userDocument.toObject<Usuarios>()

                if (userRawData != null) {
                    if(userRawData.encriptado)
                        userData.carrera = Encrypt.decrypt(userRawData.carrera).toString()
                    else
                        userData.carrera = userRawData.carrera

                    binding.btnGroup.text = userData.carrera
                }

            }
            .addOnCompleteListener {
                db.collection("groups").document(userData.carrera).collection("subgroups")
                    .whereArrayContains("users", user!!.email.toString())
                    .get()
                    .addOnSuccessListener { subgroups ->
                        val subgroupList = subgroups.toObjects(Subgroup::class.java)
                        (binding.rvSubgroups.adapter as SubgroupAdapter).setData(subgroupList)
                    }

                binding.btnGeneral.setOnClickListener {

                    db.collection("groupChat")
                        .get()
                        .addOnSuccessListener { group->

                            val listChats = group.toObjects(groupChatData::class.java)
                            for(item in listChats) {
                                if (item.name== userData.carrera) {
                                    val intent = Intent(activity, ChatGeneralActivity::class.java)
                                    intent.putExtra("groupId", userData.carrera)
                                    intent.putExtra("chatId", item.id)
                                    intent.putExtra("name",  userData.carrera)
                                    startActivity(intent)
                                }
                            }
                        }


                }
            }

    }

    private fun subgroupSelected(subgroup: Subgroup) {

        db.collection("groupChat")
            .get()
            .addOnSuccessListener { group->

                val listChats = group.toObjects(groupChatData::class.java)
                for(item in listChats) {
                    if (item.name== subgroup.id) {
                        //Aqui cambiaremos el activity
                        val intent = Intent(activity, ChatGeneralActivity::class.java)
                        //Este putExtra no jala porque hay que dividir los activities
                        intent.putExtra("subgroupId", subgroup.id)
                        intent.putExtra("chatId", item.id)
                        intent.putExtra("name", subgroup.id)
                        startActivity(intent)
                    }
                }
            }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}