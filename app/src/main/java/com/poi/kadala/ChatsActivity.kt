package com.poi.kadala

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.poi.kadala.adaptadores.MessageAdapter
import com.poi.kadala.models.Message
import com.poi.kadala.models.Usuarios
import com.poi.kadala.utils.Status
import kotlinx.android.synthetic.main.activity_chats.*
import kotlinx.android.synthetic.main.activity_chats.rv_mensajes_muro
import kotlinx.android.synthetic.main.activity_chats.myLocation
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.fragment_notifications.*
import kotlinx.android.synthetic.main.item_chat.*
import org.jitsi.meet.sdk.JitsiMeetActivity
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions
import java.io.ByteArrayOutputStream
import java.net.URI
import java.net.URL
import java.util.*


class ChatsActivity : AppCompatActivity(), MessageAdapter.OnLocationClickListener, MessageAdapter.OnFileClickListener{

    private var chatId=""
    private var otherUserId=""
    private var name=""
    private var username2=""
    private val fs = FirebaseFirestoreSettings.Builder().setPersistenceEnabled(true).build()
    private var db= Firebase.firestore.apply {
        this.firestoreSettings = fs
    }
    private val REQUEST_CODE_IMAGE = 1004
    private val REQUEST_CODE_PDF = 1005

    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chats)


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        intent.getStringExtra("chatId")?.let{chatId=it}
        intent.getStringExtra("name")?.let{name=it}
        intent.getStringExtra("username")?.let{username2=it}
        intent.getStringExtra("otherUserId")?.let{otherUserId=it}
        myLocation.text=name

        if (chatId.isNotEmpty() && name.isNotEmpty()){
            initViews()
        }

        val VideoButton=findViewById<android.widget.ImageButton>(R.id.imageButton7)

        VideoButton.setOnClickListener{
            val options = JitsiMeetConferenceOptions.Builder()
                .setServerURL(URL("https://meet.jit.si"))
                .setRoom(chatId)
                .setAudioMuted(true)
                .setVideoMuted(true)
                .setAudioOnly(false)
                .setWelcomePageEnabled(false)
                .setFeatureFlag("add-people.enabled", false)
                .setFeatureFlag("invite.enabled", false)
                .setFeatureFlag("chat.enabled", false)
                .setFeatureFlag("meeting-name.enabled", false)
                .setFeatureFlag("live-streaming.enabled", false)
                .setFeatureFlag("recording.enabled",false)
                .build()

            JitsiMeetActivity.launch(this, options)
        }
    }

    private fun initViews() {

        var user = FirebaseAuth.getInstance().currentUser?.email.toString();
        rv_mensajes_muro.layoutManager= LinearLayoutManager(this)
        rv_mensajes_muro.adapter=MessageAdapter(this,user, this, this)

        sendMessageButton.setOnClickListener { sendMessage() }
        btn_location.setOnClickListener{ sendLocation()}
        btn_image.setOnClickListener { openGalleryForImage() }
        btn_document.setOnClickListener { openGalleryForPDF() }

        val chatRef= db.collection("chats").document(chatId)

        chatRef.collection("messages").orderBy("dob", Query.Direction.ASCENDING)
            .get()
            .addOnSuccessListener { messages->
                val listMessages= messages.toObjects(Message::class.java)
                (rv_mensajes_muro.adapter as MessageAdapter).setData(listMessages)
            }

        chatRef.collection("messages").orderBy("dob", Query.Direction.ASCENDING)
            .addSnapshotListener{messages, error->
                if (error==null){
                    messages?.let {
                        val listMessages= it.toObjects(Message::class.java)
                        (rv_mensajes_muro.adapter as MessageAdapter).setData(listMessages)
                    }
                }
            }


        db.collection("usersByEmail").document(otherUserId)
            .get()
            .addOnSuccessListener { otherUserDocument ->
                val otherUser = otherUserDocument.toObject<Usuarios>()
                if (otherUser != null){
                    if(otherUser.estado) tv_status.text = "Conectado" else tv_status.text = "Desconectado"
                }
            }

        db.collection("usersByEmail").document(otherUserId)
            .addSnapshotListener{ otherUserDocument, error ->

                if(error == null && otherUserDocument != null && otherUserDocument.exists()){
                    val otherUser = otherUserDocument.toObject<Usuarios>()
                    if (otherUser != null){
                        if(otherUser.estado) tv_status.text = "Conectado" else tv_status.text = "Desconectado"
                    }

                }
            }

    }

    private fun openGalleryForImage(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_CODE_IMAGE)
    }

    private fun openGalleryForPDF(){
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        startActivityForResult(intent, REQUEST_CODE_PDF)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var user= FirebaseAuth.getInstance().currentUser?.email.toString();

        var fileName : String

        var fileURI : Uri?
        var message = Message(
            from = user
        )

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_IMAGE){

            fileName = UUID.randomUUID().toString()
            fileURI = data?.data


            val storageReference = FirebaseStorage.getInstance().getReference("uploads/images/$fileName")
            if (fileURI != null) {
                storageReference.putFile(fileURI).addOnSuccessListener {
                    storageReference.downloadUrl.addOnSuccessListener {
                        fileName = it.toString()
                        message.image = fileName
                        db.collection("chats").document(chatId).collection("messages").document().set(message)
                            .addOnSuccessListener {
                                Toast.makeText(this, "Imagen subida con exito", Toast.LENGTH_SHORT).show()
                            }

                    }.addOnFailureListener {
                        Toast.makeText(this, "Fallo en la subida de la imagen", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_PDF){

            fileName = UUID.randomUUID().toString()
            fileURI = data?.data

            val storageReference = FirebaseStorage.getInstance().getReference("uploads/files/$fileName")
            if (fileURI != null) {
                storageReference.putFile(fileURI).addOnSuccessListener {
                    storageReference.downloadUrl.addOnSuccessListener {
                        fileName = it.toString()
                        message.file = fileName

                        db.collection("chats").document(chatId).collection("messages").document().set(message)
                            .addOnSuccessListener {
                                Toast.makeText(this, "Archivo subido con exito", Toast.LENGTH_SHORT).show()
                            }
                    }.addOnFailureListener {
                        Toast.makeText(this, "Fallo en la subida del archivo", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun sendLocation() {
        val task = fusedLocationProviderClient.lastLocation

        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED
        ){
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),101)
            return
        }

        task.addOnSuccessListener {
            if (it!=null){
                var user= FirebaseAuth.getInstance().currentUser?.email.toString();
                val location= listOf(it.latitude, it.longitude)

                val message =Message(
                    message = "",
                    from = user,
                    location = location
                )

                db.collection("chats").document(chatId).collection("messages").document().set(message)

                editTextTextPersonName2.setText("")

            }
        }

    }

    private fun sendMessage() {

        var user = FirebaseAuth.getInstance().currentUser?.email.toString();
        val message =Message(
            message = editTextTextPersonName2.text.toString(),
            from = user
        )

        db.collection("chats").document(chatId).collection("messages").document().set(message)

        editTextTextPersonName2.setText("")
    }

    override fun onResume() {
        super.onResume()
        Status.setUserOnline(db,  FirebaseAuth.getInstance().currentUser!!)
    }

    override fun onPause() {
        super.onPause()
        Status.setUserOffline(db,  FirebaseAuth.getInstance().currentUser!!)

    }

    override fun onButtonClick(toString: String, toString1: String) {
   val intent= Intent(this,mapActivity::class.java)
        intent.putExtra("latitude", toString)
        intent.putExtra("longitude", toString1)
        startActivity(intent)
    }

    override fun OnFileButtonClick(position: Int, fileString: String) {
        startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(fileString)
            )
        )
    }

}