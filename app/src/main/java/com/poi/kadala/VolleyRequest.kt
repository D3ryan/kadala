package com.poi.kadala

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class VolleyRequest constructor(context: Context) {

    companion object{
        private var INSTANCE : VolleyRequest? = null

        fun getInstance(context: Context) =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: VolleyRequest(context).also {
                    INSTANCE = it
                }
            }

/*      Esto esta por desaparecer en el siguiente commit
        fun makeJsonObjectRequestGet(url: String): JsonObjectRequest {
            return JsonObjectRequest(Request.Method.GET, url, null,
                { response ->
                    Log.i("INFO", "Response is: $response")
                },
                { error ->
                    Log.e("ERROR", "There was an error: $error")

                })
        }

        fun makeJsonObjectRequestPost(url: String, jsonObject : JSONObject): JsonObjectRequest {
            return JsonObjectRequest(Request.Method.POST, url, jsonObject,
                { response ->
                    Log.i("INFO", "Response is: ${response.getString("username")}")
                    Log.i("INFO", "Response is: ${response.getString("carrera")}")
                },
                { error ->
                    Log.e("ERROR", "There was an error: $error")

                })
        }


        fun makeStringRequest(url: String): StringRequest {
            return StringRequest(Request.Method.GET, url,
                { response ->
                    Log.i("INFO", "Response is: $response")
                },
                { error ->
                    Log.e("ERROR", "There was an error: $error")

                })
        }
        */

    }

    val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context.applicationContext)
    }

    fun <T> addToRequestQueue(request: Request<T>){
        requestQueue.add(request)
    }

}