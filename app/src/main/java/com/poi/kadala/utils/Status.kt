package com.poi.kadala.utils

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

object Status {

    fun setUserOnline(db : FirebaseFirestore, user : FirebaseUser){
        db.collection("usersByEmail").document(user?.email.toString()).update("estado", true)
    }
    fun setUserOffline(db : FirebaseFirestore, user : FirebaseUser){
        db.collection("usersByEmail").document(user?.email.toString()).update("estado", false)
    }

}