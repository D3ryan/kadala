package com.poi.kadala

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.poi.kadala.databinding.ActivityMainBinding
import org.json.JSONObject

class MainActivity : AppCompatActivity(), View.OnClickListener{

    private lateinit var binding: ActivityMainBinding
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnLogin.setOnClickListener(this)
        auth = Firebase.auth



        //Aqui lo cambie a que sea con el click listener y con el binding

        /* setContentView(R.layout.activity_main)

       val loginButton=findViewById<android.widget.Button>(R.id.btn_login)

       loginButton.setOnClickListener{
            val ventanaMenu: android.content.Intent = android.content.Intent(this, MenuActivity::class.java)
            startActivity(ventanaMenu)
        }*/
    }

    override fun onClick(v: View?) {
        when(v!!.id){
           R.id.btn_login->{
               val email :String = binding.user.text.toString()
               val password :String = binding.contra.text.toString()
               signIn(email, password)

                /*   Falta hacer el script de php en XAMPP pero creo que esto ya puede funcionar
                val jsonObject = JSONObject()
                jsonObject.put("email", binding.user.text)
                jsonObject.put("password", binding.contra.text)

                val jsonObjectRequest : JsonObjectRequest = VolleyRequest.makeJsonObjectRequestPost("url", jsonObject)
                VolleyRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)*/

            }
        }
    }

    private fun signIn(email: String, password: String){

        if(email.isNotEmpty() && password.isNotEmpty()){
            auth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this){ task->
                    if(task.isSuccessful){
                        val currentUser = auth.currentUser

                        //Si es correcto nos pasara al otro activity
                        if(currentUser!= null){
                            val intent= Intent(this, MenuActivity::class.java)
                            intent.putExtra("user", currentUser.email)
                            startActivity(intent)
                            finish()
                        }
                    }
                    else
                        Toast.makeText(this, "No se ha podido autenticar con los datos ingresados", Toast.LENGTH_SHORT).show()
                }
        }
        else
            Toast.makeText(this, "Alguno de los campos esta sin llenar", Toast.LENGTH_SHORT).show()

    }

}