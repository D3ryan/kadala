package com.poi.kadala

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.facebook.react.modules.intent.IntentModule
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.poi.kadala.databinding.ActivityMenuBinding
import com.poi.kadala.services.StatusService
import com.poi.kadala.utils.Status

class MenuActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMenuBinding
    private var user=""

    private var db= Firebase.firestore
    private var loggedUser =  FirebaseAuth.getInstance().currentUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        startService(Intent(application, StatusService::class.java))

        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)


//        db.collection("usersByEmail").document(loggedUser?.email.toString()).update("estado", true)

        intent.getStringExtra("user")?.let {user=it}
        val navView: BottomNavigationView = binding.navView

        //val navController = findNavController(R.id.nav_host_fragment_activity_menu)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_menu) as NavHostFragment
        val navController = navHostFragment.navController

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications, R.id.navigation_chat
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


    }


    override fun onResume() {
        super.onResume()
        Status.setUserOnline(db,  FirebaseAuth.getInstance().currentUser!!)
    }

    override fun onPause() {
        super.onPause()
        Status.setUserOffline(db,  FirebaseAuth.getInstance().currentUser!!)

    }




}