package com.poi.kadala.models

data class Usuarios (
    var id: String = "",
    var username: String = "",
    var carrera: String="",
    var password: String = "",
    var estado: Boolean = true,
    var encriptado : Boolean = true
)