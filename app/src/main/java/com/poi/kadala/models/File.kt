package com.poi.kadala.models

import java.net.URI

class File(
    var fileName: String = "",
    var fileUrl: String = ""
)