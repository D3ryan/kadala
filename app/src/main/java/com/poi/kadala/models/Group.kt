package com.poi.kadala.models

data class Group(
    var id: String = "",
    var subgroups: ArrayList<Subgroup>? = null
)