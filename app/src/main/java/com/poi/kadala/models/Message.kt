package com.poi.kadala.models

import java.util.*

 data class Message(
     var message: String = "",
     var from: String = "",
     var dob: Date = Date(),
     var location: List<Double> = emptyList(),
     var image: String = "",
     var file : String = ""
 )